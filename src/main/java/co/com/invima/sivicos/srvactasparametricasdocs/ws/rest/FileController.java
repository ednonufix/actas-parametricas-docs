package co.com.invima.sivicos.srvactasparametricasdocs.ws.rest;

import co.com.invima.canonicalmodelsivico.dtosivico.GenericResponseDTO;
import co.com.invima.sivicos.srvactasparametricasdocs.service.ProcessService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

/*
    User:Eduardo Noel<enoel@soaint.com>
    Date: 31/8/21
    Time: 12:55
*/
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/excel/")
@Slf4j
public class FileController {

    private final ProcessService service;

    @PostMapping("tags")
    public ResponseEntity<GenericResponseDTO> getListaTags(@RequestBody Map<String,String> base64) throws IOException {

        return ResponseEntity.status(HttpStatus.OK).body(service.getListaEtiquetas(base64.get("base64")));

    }

    @PostMapping("pdf")
    public ResponseEntity<GenericResponseDTO> getPDF(@RequestBody Map<String,Object> datos) throws IOException, InvalidFormatException {

        return ResponseEntity.status(HttpStatus.OK).body(service.getPDF(datos));

    }

}
